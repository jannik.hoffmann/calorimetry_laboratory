from typing import List, Tuple

import h5py as h5
import matplotlib.pyplot as plt
import numpy as np


def plot_temp_over_time(
    data: List[np.ndarray],
    time: List[np.ndarray],
    legend: List[str],
    x_label: str,
    y_label: str,
) -> None:
    """Plots temperature data over time with error bars for multiple datasets.

    This function creates a plot representing temperature data against time
    for multiple sensors or datasets. Each dataset's standard deviation is visualized
    with error bars.

     Args:
        data (List[np.ndarray]): A list of numpy arrays where each array represents
                                 the temperature data (with standard deviation).
                                 Each array should have a shape of (2, n), with n
                                 representing the number of data points.
        time (List[np.ndarray]): A list of numpy arrays with the time data corresponding
                                 to each dataset in `data`.
        legend (List[str]): A list of strings that label each dataset in the legend.
        x_label (str): The label for the x-axis (time).
        y_label (str): The label for the y-axis (temperature).

    """
    # init the matplotlib.axes.Axes and matplotlib.figure.Figure Object for later plot
    fig, ax = plt.subplots(1, 1)
    markers = ["o", "^", "2", "p", "D"]

    data_temperature = data[0]
    data_standartabweichung = data[1]
    
    for i in range(len(data_temperature)):
        # TODO: draw a plot using the ax.errorbar(...) function
        # Document: https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.errorbar.html
        ax.errorbar(x=time[i], y=data_temperature[i], yerr=data_standartabweichung[i], xerr=None, fmt='', ecolor=None, elinewidth=None, capsize=None, barsabove=False, lolims=False, uplims=False, xlolims=False, xuplims=False, errorevery=1, capthick=None)

        # DONE #

    # Errorbars removed from Legend
    legend_handles, labels = ax.get_legend_handles_labels()
    legend_handles = [h[0] for h in legend_handles]

    # TODO: set legend, x- and y- axis label.
    ax.legend(handles=legend_handles)
    ax.set_xlabel(x_label, fontdict=None, labelpad=None, loc=None)
    ax.set_ylabel(y_label, fontdict=None, labelpad=None, loc=None)
    # DONE #

    ax.ticklabel_format(scilimits=(0, 3))


def get_plot_data_from_dataset(
    data_path: str, group_path: str
) -> dict[str, np.ndarray]:
    """Get the necessary data from the dataset to plot.

    This function returns the data in a HDF5 file in all subgroups of a group in 'group_path'
    and automatically categorizes and names the data based on the name of the dataset as well as the metadata.

    Args:
        data_path (str): path to HDF5 file.
        group_path (str): path in HDF5 to group.

    Returns:
        dict[str, np.ndarray]: Data for plot in a dict.

    Example:
        Output (example data):
        {
            "temperature": np.array([
                            [24.89, 24.92, 24.00, 25.39],
                            [24.89, 24.92, 24.00, 25.39],
                            [24.89, 24.92, 24.00, 25.39]
                        ]) -> temperature from each sensor, The first dimension(row) represents the sensor.
            "timestamp": np.array([
                            [0.43, 1.60, 3.05, 4.25],
                            [0.81, 2.13, 3.49, 4.62],
                            [1.34, 2.60, 3.85, 5.08],
                        ]) -> timestamp for each sensor, The first dimension(row) represents the sensor.
            "name": np.array(["sensor_1", "sensor_2", "sensor_3"]) -> name of each sensor should be hier
        }

    """
    temperature = []
    time = []
    name = []

    with h5.File(data_path) as data:
        group = data[group_path]
        subgroups = []
        min_len = None
        start_time = None

        for subgroup in group:
            try:
                dataset_start_time = group[subgroup]["timestamp"][0]
                dataset_len = len(group[subgroup]["timestamp"])

                # Find the minimum length of the data set.
                if min_len is None:
                    min_len = dataset_len
                elif dataset_len < min_len:
                    min_len = dataset_len

                subgroups.append(subgroup)

                # Only group with dataset called timestamp will be read.
            except KeyError:
                continue

            # TODO: Find the start time point of the measurement.
        for subgroup in group:
            try:
                dataset_start_time = group[subgroup]["timestamp"][0]

                if start_time is None:
                    start_time = dataset_start_time
                elif dataset_start_time < start_time:
                    start_time = dataset_start_time

                # Only group with dataset called timestamp will be read.
            except KeyError:
                continue

            # DONE #

        for subgroup in subgroups:
            # TODO: Save data in to the lists temperature, time and name.
            # Data for each sensor must have the same length because of np.ndarray will be use in the output.
            try:
                dataset_len_2 = len(group[subgroup]["timestamp"])
                data_array_time=np.array(group[subgroup]["timestamp"]) 
                data_array_temperature=np.array(group[subgroup]["temperature"])   
    
                # Find the minimum length of the data set.
                if dataset_len_2 > min_len:
                    data_array_time = np.resize(data_array_time, data_array_time.size - 1)
                    data_array_temperature = np.resize(data_array_temperature, data_array_temperature.size - 1)   
    
                
                temperature.append(data_array_temperature)
                time.append(data_array_time)
                i = data["RawData"]
                j = i[subgroup]
                subgroup_name = j.attrs["name"]
                name.append(subgroup_name)
                    
            except KeyError:
                continue
   

            # DONE #

    # TODO: return the output dict.
    dict = {
        'temperature' : temperature,
        'time': time,
        'name': name
    }

    return dict

    # DONE #


def cal_mean_and_standard_deviation(data: np.ndarray) -> np.ndarray:
    """Calculating mean and standard deviation for raw data of multiple sensors.

    Args:
        data (np.ndarray): raw data in a 2 dimensional array (m, n), the first dimension should not be 1 if
                           there are multiple measurements at the same time (and place).

    Returns:
        np.ndarray: mean of raw data with standard deviation in a 2D ndarray with shape (2, n).

    """
    # TODO: Calculate the mean and standard deviation of the first dimension and return the result as a
    # two-dimensional (2, n)-shaped ndarray.

    array_time = np.arange(0, len(data), 2)
    array_temperature = np.arange(1, len(data), 2)
    times_list = []
    for a in array_time:
        z = int(a)
        times_list.append(data[z])
    temperature_list = []
    for a in array_temperature:
        z = int(a)
        temperature_list.append(data[z])

    
    aktuelle_temperature = []
    durchschnitt_temperature = []
    standartabweichung_temperature = []
    h = 0
    while h < len(data[0]):
        aktuelle_temperature = []
        for s in temperature_list:
            aktuelle_temperature.append(s[h])
        durchschnitt = np.sum(aktuelle_temperature)/len(aktuelle_temperature)
        durchschnitt_temperature.append(durchschnitt)
        standartabweichung = np.std(aktuelle_temperature, axis=None, dtype=None, out=None, ddof=0)
        standartabweichung_temperature.append(standartabweichung)
        h += 1

    
    aktuelle_zeit = []
    durchschnitt_zeit = []
    h = 0
    while h < len(data[0]):
        aktuelle_zeit = []
        for s in times_list:
            aktuelle_zeit.append(s[h])
        durchschnitt = np.sum(aktuelle_zeit)/len(aktuelle_zeit)
        durchschnitt_zeit.append(durchschnitt)
        h += 1

    array_1 = [durchschnitt_temperature, standartabweichung_temperature]
    ausgang = [array_1, durchschnitt_zeit]
    return(ausgang)

    # DONE #


def get_start_end_temperature(
    temperature_data: np.ndarray, threshold: float = 0.05
) -> Tuple[float, float]:
    """Calculates the high and low temperatures from a dataset.

    This function computes the (average of) the highest temperatures and the (average of) the lowest temperatures
    within a given threshold from the maximum and minimum temperatures recorded in the dataset. These are
    considered as the ending and starting temperatures respectively.

    Args:
        temperature_data (np.ndarray): The temperature dataset as a 2D numpy array.
        threshold (float): The threshold percentage used to identify temperatures close to the maximum
                           and minimum values as high and low temperatures respectively. Default to 0.05

    Returns:
        Tuple[float, float]: A tuple containing the average high temperature first and the average low
                             temperature second.

    """
    # TODO: You don't have to implement this function exactly as docstring expresses it, it just gives
    # an idea that you can refer to. The goal of this function is to obtain from the data the high and
    # low temperatures necessary to calculate the heat capacity.


    min_temperature = min(temperature_data)
    max_temperature = max(temperature_data)
    array = [max_temperature, min_temperature]

    return array

    # DONE #
